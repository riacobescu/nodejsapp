'use strict';
var AWS = require("aws-sdk");

module.exports.hello = async (event) => {
    const lambda = new AWS.Lambda({
        region: "us-east-1"
    });

    const emails = [
        "a@y.com",
        "b@y.com"
    ];

    emails.forEach((mail) => {
        const params = {
            FunctionName: "email-service-dev-send",
            InvocationType: "RequestResponse",
            Payload: JSON.stringify({email:mail})
        };

        return lambda.invoke(params, function(error, data) {
                if (error) {
                    console.error(JSON.stringify(error));
                    return new Error(`Error printing messages: ${JSON.stringify(error)}`);
                } else if (data) {
                    console.log(data);
                    return data;
                }

            });
        });

};
