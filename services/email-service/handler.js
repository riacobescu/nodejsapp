'use strict';
const nodemailer = require("nodemailer");

module.exports.email = async (event, context) => {
    const transporter = nodemailer.createTransport({
        host: "smtp.ethereal.email",
        port: 587,
       // secure: true, // upgrade later with STARTTLS
        auth: {
            user: 'taylor.ruecker@ethereal.email',
            pass: '6NjkV5A8Ert5zeedx7'
        }
    });
    let to = event.email;

    var message = {
        from: "sender@server.com",
        to: to,
        subject: "Message title",
        text: "Plaintext version of the message",
        html: "<p>HTML version of the message</p>"
    };

    let response = {
        statusCode: 200,
            body: {
                message: 'Go Serverless v1.0! Your function executed successfully! Email One',
                input: event,
                responses: null
            }
    };

    const info = new Promise(function(resolve, reject) {
        transporter.sendMail(message, function(err, info){
            if(err){
                console.log(err)
                reject(Error(err))
            }
            if(info){
                console.log('Success sent to {to} message Id' + info.messageId)
                response.body.responses = 'Success sent to {to} message Id' + info.messageId;
            }


            resolve(response)

        });
    })

    return info;
    // Use this code if you don't use the http event with the LAMBDA-PROXY integration
    // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};